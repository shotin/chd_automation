<?php
// [9:45 AM, 1/19/2022] Engr Kayode: Assume this is the URL of the API: http://localhost:6060/schools/paymentsetup
// [9:45 AM, 1/19/2022] Engr Kayode: The HTTP method is POST
// [9:45 AM, 1/19/2022] Engr Kayode: This is a smple payload
// [9:45 AM, 1/19/2022] Engr Kayode: 
// {
//     "schoolIntegrationRequests": [
//         {
//             "schoolUniqueId": "1648",
//             "paymentGateway": "interswitch",
//             "bankDetails": [
//                 {
//                     "bankCode": "117",
//                     "isoCode": "",
//                     "accountNumber": "1017721638",
//                     "accountName": "CORNELIA INTL SCHOOL LTD",
//                     "purpose": "School Fees",
//                     "mobile": "09088765066",
//                     "countryCode": "NG",
//                     "accountNumberType": "SAVINGS",
//                     "currencyCode": "NGN",
//                     "isDomiciliary": false
//                 }
//             ]
//         }
//     ]
// }
// [9:46 AM, 1/19/2022] Engr Kayode: Write code for an API to do call this and pass JSON as the body
// [9:46 AM, 1/19/2022] Engr Kayode: Sample code you can use for calling an API is :
// [9:46 AM, 1/19/2022] Engr Kayode: 
class Post {
public function verifyStudent($schoolRegisteredStateId_sisi, $dateOfBirth)
    {
        
     $edvesResponseArray = array(
         'status'=>false, 
         'message'=>'API Call Has Not Yet Been Made', 
         'surname'=> "Some Surname", 
         "othernames"=> "Othernames",
          "photo"=> ""
        );
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://dawn-eup.org/api-dev/v2/vendor.php/verifystudent',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{
            "terminal_id": "' . $this->terminal_id . '",
            "secret_key": "' . $this->secret_key . '",
            "sisi": "'.$schoolRegisteredStateId_sisi.'",
            "date_of_birth": "'.$dateOfBirth.'"
        }',
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        if (trim($response) == "")
       {
            return $edvesResponseArray;       
       }
        $responseArray = json_decode($response, true);
        
        $edvesResponseArray['status'] = $responseArray['status'];
        $edvesResponseArray['message'] = $responseArray['message'];
        $edvesResponseArray['surname'] = $responseArray['data']['surname'];
        $edvesResponseArray['othernames'] = $responseArray['data']['othernames'];
        $edvesResponseArray['photo'] = $responseArray['data']['photo'];
        return $edvesResponseArray;
    }
}