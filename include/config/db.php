<?php

class Database {

	private $conn;

	protected function connect() {

			$host = "localhost";
			$user =	"root";
			$password ="";
			$dbname = "edves";

			$dsn = "mysql:host=" . $host . ";dbname=" . $dbname;

			try {
			  $this->conn = new PDO($dsn, $user);
			  $this->conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
			  $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				return $this->conn;
			} catch (PDOException $e) {
				echo "Connection failed: " .$e->getMessage();
			}

	}

}


?>