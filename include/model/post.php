<?php
  class Post extends Database {
      private $conn;

      public function __construct() 
      {
         $this->conn = $this->connect();
      }
//id, school, payment, bank, number, account_name, account_purpose
      public function verifyStudent($school, $payment, $bank,  $account_number,  $account_name,  $account_purpose)
      {
        $edvesResponseArray =  [  
          '{
            "status" =>                 "true", 
            "message":                  "API Call Has Not Yet Been Made", 
            "school":                   "'.$school.'", 
            "payment":                  "'.$payment.'",
             "bank":                    "'.$account_number.'",
             "account_number":          "'.$account_number.'",
             "account_name":            "'.$account_name.'",
             "account_purpose":         "'.$account_purpose.'",
           }',
          ];
          

           $curl = curl_init();
        
           curl_setopt_array($curl, array(
             CURLOPT_URL => 'https://dawn-eup.org/api-dev/v2/vendor.php/verifystudent',
             CURLOPT_RETURNTRANSFER => true,
             CURLOPT_ENCODING => '',
             CURLOPT_MAXREDIRS => 10,
             CURLOPT_TIMEOUT => 0,
             CURLOPT_FOLLOWLOCATION => true,
             CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
             CURLOPT_CUSTOMREQUEST => 'POST',
             CURLOPT_POSTFIELDS => json_encode($edvesResponseArray),
             // CURLOPT_HTTPHEADER => array(
             //      "Content-Type: application/json",
             //      "Authorization:"
             //  ),

           ));

           $response = curl_exec($curl);

           curl_close($curl);

           $responseArray = json_decode($response, true);

           $edvesResponseArray['status'] = $responseArray['status'];
           $edvesResponseArray['school'] = $responseArray['school'];
           $edvesResponseArray['payment'] = $responseArray['data']['payment'];
           $edvesResponseArray['bank'] = $responseArray['customer']['bank'];
           $edvesResponseArray['account_number'] = $responseArray['data']['account_number'];
           $edvesResponseArray['account_name'] = $responseArray['data']['account_name'];
           $edvesResponseArray['account_purpose'] = $responseArray['data']['account_purpose'];
           return $edvesResponseArray;

           // $status = $res->data->status;
           // $school = $res->data->school;
           // $payment = $res->data->tx_ref;
           // $bank= $res->data->customer->bank;
           // $account_number = $res->data->customer->account_number;
           // $account_name = $res->data->customer->account_name;
           // $account_purpose = $res->data->customer->account_purpose;
        //    if (trim($response) == "")
        //   {
        //        return $edvesResponseArray;       
        //   }
           
        if($responseArray->status == true)
        {

            $sql = "INSERT INTO student(school, payment, bank, account_number, account_name, account_purpose) VALUES(?,?,?,?,?,?)";
            $stmt = $this->conn->prepare($sql);
            try {
                $stmt->execute([$school, $payment, $bank, $account_number,  $account_name,  $account_purpose]);
                return true;
            } catch(Exception $e) {
                echo $e->getMessage();
                return false;
            }
        }else
        {
            echo 'Payment not Successful';
        }
      }
  }